function demodulatedBits = demodulate(receivedSymbols,params,symbols,sizePadding)

type = params.codeType;
mods = params.mod;
M = params.niveles;
order = params.order;
no =  params.no;

if(strcmp(type,'LDPC'))
    
    if(strcmp(mods,'qam'))
        demodulatedBits = qamdemod(receivedSymbols.',M,order,'OutputType','approxllr', ...
            'UnitAveragePower',false,'NoiseVariance',no/2);
    else
        demod = comm.PSKDemodulator(M, 'PhaseOffset',0,'BitOutput',true,...
           'DecisionMethod','Approximate log-likelihood ratio', ...
           'Variance', no/2);
        demodulatedBits = step(demod,receivedSymbols.');
    end
    
else
    m = length(symbols);
    receivedSymbols = receivedSymbols.';
    
    auxReceived = repmat(receivedSymbols,1,m);
    auxSymbols = repmat(symbols,length(receivedSymbols),1);
    
    distances = abs(auxReceived - auxSymbols);
    [~,pos] = min(distances,[],2);
    
    demodulatedBits = de2bi(pos-1,'left-msb').';
    demodulatedBits = demodulatedBits(:).';
end

demodulatedBits(end-sizePadding+1:end) = [];



