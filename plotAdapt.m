
load datosAdapt.mat;

hFig = fullfig('Name', 'Codificacion Adaptativa');

plot(ind,ber','LineWidth',2);
ylabel('BER','FontSize',16);
xlabel('Tasa Tx. (simb/uso)','FontSize',20);
str = {'0 dB', '2 dB','5 dB','8 dB','10 dB','15 dB'};
posX = [2.25 2.25 2.25 2.75 2.75 4.25];
posY = [0.303 0.253 0.175 0.11 0.047 0.04];
text(posX,posY,str,'FontSize',19,'FontWeight','Bold');
grid on;
set(gca,'XTickLabel',[]);
set(gca,'FontSize',16);
vec_pos = get(get(gca, 'XLabel'), 'Position');
set(get(gca, 'XLabel'), 'Position', vec_pos + [0 -0.025 0]);

set(hFig, 'ToolBar', 'none');
movegui(hFig,'center')
a.FontSize = 0;
hFig.UserData = a;

button = zeros(1,length(ind));
pos = [0.115 0.202 0.2635 0.292 0.375 0.48 0.547 0.72 0.885];

for ii=1:length(ind)
    %Add pushbutton to view data
    button(ii) =uicontrol('Parent',hFig,'Style','pushbutton','String',num2str(ind(ii)),...
        'FontSize',17,'Units','normalized','Position',[pos(ii) 0.069 0.03 0.035],...
        'BackgroundColor',[0.96 0.96 0.96], 'Visible','on', ...
        'CallBack', {@showData,ind,numNiveles,rates});
end

function showData(PushButton, ~,ind,numNiveles,rates)

    a = PushButton.Parent.UserData;
    if(a.FontSize)
        delete(a);
    end
    value = str2num(get(PushButton, 'String'));
    index = find(ind==value);
    mod = 'QAM';
    m = numNiveles(index);
    if (m<9)
        mod = 'PSK';
    end
    strRate = rates{index};
    info = sprintf('%d-%s/LDPC %s',m,mod,strRate);
    a = text(value-0.25,0.01,info,'FontSize',16,'Color',[0.6 0. 0.1],'FontWeight','Bold');
    PushButton.Parent.UserData = a;
end