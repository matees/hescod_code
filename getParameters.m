function [Eb, minDist,tasa,warning] = getParameters(modulation, codingMethod, infoCoding)

distLDPCs = [12 9 6 4];

warning = 0;
Mv = [2 4 8 4 8 16 16 64 256];
modv = {'pam','pam','pam','psk','psk','psk','qam','qam','qam'};
ind1 = find(modulation);
m = Mv(ind1(1));
mod = modv{ind1(1)};

switch mod
    case 'pam'
        Eb = (m^2-1)/3;
    case 'psk'
        Eb = 1;
    case 'qam'
        symbols = qammod(0:m-1,m);
        Eb = mean(abs(symbols).^2);       
end
  
ind2 = find(codingMethod);
codv = {'no','ham','conv','ldpc'};
cod = codv{ind2(1)};

switch cod
    case 'no'
        minDist = 0;
        rate = 1;
        
    case 'ham'
        minDist = 3;
        info = infoCoding{ind2(1)-1};
        lv = [2 3 4];
        ind = find(info);
        l = lv(ind(1));
        n = 2^l -1;
        k = n-l;
        rate = k/n;
        
    case 'conv'
        info = infoCoding{ind2(1)-1};
        bins = zeros(1,length(info));
        for ii = 1:length(info)
            bins(ii) = length(oct2poly(info(ii)));
        end
        M = max(bins);
        t = poly2trellis(M,info);
        try
            aux = distspec(t);
            minDist = aux.dfree;
        catch
            minDist = 0;
            warning=1;
        end
        rate = 1/length(info);
        
    case 'ldpc'   
        info = infoCoding{ind2(1)-1};
        rv = [1/2 2/3 3/4 5/6];
        ind = find(info);
        rate = rv(ind(1));
        minDist = distLDPCs(ind(1));
end

tasa = log2(m)*rate;


