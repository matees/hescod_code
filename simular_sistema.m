function [] = simular_sistema(sourceBits,modulations,order,coding,infoCoding,SNRdB,showConst)

% Initilization of the selected transmission configurations
params = system_parameters(modulations,order,coding,infoCoding);

% Variables initilization
str = cell(size(params));
thr = 10^-4;

% Create the figure to plot the obtained BER curves
hfig = fullfig('Name', 'Curvas BER');

for ii = 1:length(params)

    %Compute BER values for the different transmission configurations
    [ber,m,r,symbols] = calcular_ber(sourceBits,SNRdB,params{ii});
    
    %Post-processing to polish the BER curves for LDPC codes
    auxSNR = SNRdB;
    if(strcmp(params{ii}.codeType,'LDPC'))
        pos = find(ber<thr);
        newSNRdB = SNRdB(pos(1))-0.8:0.1:SNRdB(pos(1))+0.4;
        [newBer,~,~,~] = calcular_ber(sourceBits,newSNRdB,params{ii});
        ber = [ber(1:pos-1) newBer];
        SNRdB = [SNRdB(1:pos-1) newSNRdB];
    end
    
    % Plot BER curves
    figure(1)
    semilogy(SNRdB,ber,'LineWidth',2);
    hold on;
    
    % Set legend components
    order = params{ii}.order;
    str{ii} = sprintf('%s %s + %d-%s (%s)',params{ii}.codeType, ...
                        params{ii}.legendExtra, params{ii}.niveles, ...
                        upper(params{ii}.mod), [upper(order(1)) order(2:end)]);
    
    %Plot the constellation of received symbols (if selected)
    if((showConst) && strcmp(params{ii}.codeType,'NoCoding'))
       plotReceivedSymbols(m,r,symbols,SNRdB,params{ii});
    end
    
    SNRdB = auxSNR;
end

% Code to adjust settings in the figures
figure(1)
hold off;
ylabel('BER','FontSize',16);
xlabel('SNR (dB)','FontSize',18);
grid on;
set(gca,'FontSize',16);
pbaspect([1.5 1 1]);
set(hfig, 'ToolBar', 'none');
title('Curvas BER','FontSize',20,'FontWeight','Bold');
vec_pos = get(get(gca, 'title'), 'Position');
set(get(gca, 'title'), 'Position', vec_pos + [0 vec_pos(2)/2.5 0]);
legend(str);
set(get(gca, 'Legend'), 'FontSize', 20);

