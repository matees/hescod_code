function Hes = genHexp(h,z)

  [nr, nc] = size(h);

  % Expand the base matrix h
  He = zeros(nr*z,nc*z);
  id = eye(z);
  
  for i = 1:nr
    for j = 1:nc
      if(h(i,j)>=0)
        He(z*(i-1)+1:z*i,z*(j-1)+1:z*j) = ...
            circshift(id,[0,h(i,j)]);
      end
    end
  end
  
  % Sparse matrix
  Hes = sparse(He);
  