function [ber,m,r,symbols] = calcular_ber(sourceBits,SNRdB,params)

SNRs = 10.^(SNRdB/10);
ber = zeros(1,length(SNRs));
m = {};
r = {};

for jj = 1:length(SNRs)
    
    %Encode source bits
    [encodedBits,rate,size1] = encodingOp(sourceBits,params);
    
    % Modulate encoded bits
    [modulatedSymbols,k,symbols,size2] = modulate(encodedBits,params);
    
    % Compute the noise variace according to the SNR value
    Es = mean(abs(modulatedSymbols).^2);
    Eb = Es/k;
    no = (Eb / SNRs(jj))/rate;
    params.no = no;
    
    % Add Gaussian noise
    if(isreal(modulatedSymbols))
        noise = sqrt(no/2)*randn(size(modulatedSymbols));
    else
        noise = sqrt(no/2)*(randn(size(modulatedSymbols))+ 1j*randn(size(modulatedSymbols)));
    end
    
    %Received symbols from AWGN channels
    receivedSymbols = modulatedSymbols + noise;
    
    % Demodulate receive symbopls
    demodulatedBits = demodulate(receivedSymbols,params,symbols,size2);
    
    % Decode detected symbols
    estBits = decodingOp(demodulatedBits,params,size1);
    
    % Calculate bit error probability
    ber(jj) = sum(abs(sourceBits-estBits))/length(sourceBits);
    if(~ber(jj))
        break;
    end
    
    %Save data to plot constellation of received symbols
    m{jj} = modulatedSymbols;
    r{jj} = receivedSymbols;
end
