function [encodedBits,rate,sizePadding] = encodingOp(sourceBits,params)

type = params.codeType;
sizePadding = 0;

switch type 
    
    case 'NoCoding'
        encodedBits = sourceBits;
        rate = 1;
        
    case 'Hamming'
        l = params.info;
        k = 2^l - 1 - l;
        n = 2^l-1;
        [~,g] = hammgen(l);
        padding = mod(length(sourceBits),k);
        if(padding)
            sizePadding = k - padding;
            sourceBits = [sourceBits zeros(1,sizePadding)];
        end
        bits = reshape(sourceBits,k,[]).';
        encodedBits = mod(bits*g,2).';
        encodedBits = encodedBits(:).';
        rate = k/n;
        
    case 'Conv.'
        info = params.info;
        bins = zeros(1,length(info));
        for ii = 1:length(info)
            bins(ii) = length(oct2poly(info(ii)));
        end
        M = max(bins);
        t = poly2trellis(M,info);
        encodedBits = convenc(sourceBits,t);
        rate = 1/length(info);
        
    case 'LDPC'
        hb = params.info;
        z = 80;
        H = genHexp(hb,z);
        [m,n] = size(H);
        k = n-m;
        rate = k/n;
        padding = mod(length(sourceBits),k);
        if(padding)
            sizePadding = k - padding;
            sourceBits = [sourceBits zeros(1,sizePadding)];
        end
        nb = length(sourceBits)/k;
        encodedBits = zeros(1,n*nb);
        
        for ii=1:nb
            %Encode the source bits with a ldpc encoder
            blockii = sourceBits((ii-1)*k+1:ii*k);
            encodedBits((ii-1)*n+1:ii*n) = ldpc_encoder(H,blockii,z);
        end 
end