function [] = simular_Imagen(sourceBits,modulations,order,coding,infoCoding,SNRdB)

params = system_parameters(modulations,order,coding,infoCoding);
SNRs = 10.^(SNRdB./10);

for ii = 1:length(params)
    
    for jj = 1:length(SNRdB)
        
        %Code source bits
        [encodedBits,rate,size1] = encodingOp(sourceBits,params{ii});
        
        % Modulate encoded bits
        [modulatedSymbols,k,symbols,size2] = modulate(encodedBits,params{ii});
        
        % Compute the noise variace according to the SNR value
        Es = mean(abs(modulatedSymbols).^2);
        Eb = Es/k;
        no = (Eb / SNRs(jj))/rate;
        params{ii}.no = no;
        
        % Add Gaussian noise
        if(isreal(modulatedSymbols))
            noise = sqrt(no/2)*randn(size(modulatedSymbols));
        else
            noise = sqrt(no/2)*(randn(size(modulatedSymbols))+ 1j*randn(size(modulatedSymbols)));
        end
        receivedSymbols = modulatedSymbols + noise;
        
        % Demodulate receive symbopls
        demodulatedBits = demodulate(receivedSymbols,params{ii},symbols,size2);
        
        % Decode detected symbols
        estBits = decodingOp(demodulatedBits,params{ii},size1);
        
        %Recover the original image        
        br = reshape(sourceBits,8,[]);
        br = br.';
        dataAux = bi2de(br,'left-msb');
        
        %Show the original image
        str = sprintf('Transmision de la imagen fuente usando %d-%s y SNR = %d dB',...
                        params{ii}.niveles, params{ii}.mod, SNRdB(jj));
        fullfig('Name', str);
        data = reshape(dataAux,512,[]);
        h1 = subplot(1,2,1);
        imshow(uint8(data));
        
        %Legend 
        l = size(data,2);
        text(l/3-10,-30,'Imagen Original','FontSize',30,'FontWeight','Bold');
        
        % Recover image from the received bits
        br = reshape(estBits,8,[]);
        br = br.';
        dataAux = bi2de(br,'left-msb');
        
        %Show the received image
        data = reshape(dataAux,512,[]);
        h2 = subplot(1,2,2);
        imshow(uint8(data));
        set(h1,'Position',[-0.11 0.13 0.75 0.75]);
        set(h2,'Position',[0.36 0.13 0.75 0.75]);
        
        %Legend 
        l = size(data,2);
        text(l/3-10,-30,'Imagen Recibida','FontSize',30,'FontWeight','Bold');        
    end
end


