function params = system_parameters(modulations,order,coding,infoCoding) 

load ieee802_16e_matrices.mat;
hbMatrices={ieee802_16e_1_2,ieee802_16e_2_3A,ieee802_16e_3_4A,ieee802_16e_5_6};
rates = {'1/2','2/3','3/4','5/6'};

%Mapping structure
mods = {'pam','pam','pam','psk','psk','psk','qam','qam','qam'};
niveles = [2 4 8 4 8 16 16 64 256];
codeType = {'NoCoding','Hamming','Conv.','LDPC'};

ind = find(modulations);
ind2 = find(coding);

%Build the different transmission configurations
params = {};
cont = 1;
for ii = 1:length(ind)
    for jj = 1:length(ind2)
        switch codeType{ind2(jj)}
            case 'NoCoding'
                params{cont}.mod = mods{ind(ii)};
                params{cont}.niveles = niveles(ind(ii));
                params{cont}.order = order;
                params{cont}.codeType = codeType{ind2(jj)};
                params{cont}.legendExtra = '';
                cont = cont+1;
            case 'Conv.'
                params{cont}.mod = mods{ind(ii)};
                params{cont}.niveles = niveles(ind(ii));
                params{cont}.order = order;
                params{cont}.codeType = codeType{ind2(jj)};
                params{cont}.info = infoCoding{2};
                params{cont}.legendExtra = '';
                cont = cont+1;
            case 'Hamming'
                lValues = [2 3 4]; 
                ind3 = find(infoCoding{1});
                for zz = 1:length(ind3)
                    params{cont}.mod = mods{ind(ii)};
                    params{cont}.niveles = niveles(ind(ii));
                    params{cont}.order = order;
                    params{cont}.codeType = codeType{ind2(jj)};
                    params{cont}.info = lValues(ind3(zz));
                    params{cont}.legendExtra = sprintf('(%d)',lValues(ind3(zz)));
                    cont = cont+1;
                end
            case 'LDPC'
                if(~(strcmp(mods{ind(ii)},'pam') && niveles(ind(ii))>2))
                    ind3 = find(infoCoding{3});
                    for zz = 1:length(ind3)
                        params{cont}.mod = mods{ind(ii)};
                        params{cont}.niveles = niveles(ind(ii));
                        params{cont}.order = order;
                        params{cont}.codeType = codeType{ind2(jj)};
                        params{cont}.info = hbMatrices{ind3(zz)};
                        params{cont}.legendExtra = sprintf('(%s)',rates{ind3(zz)});
                        cont = cont+1;
                    end
                end
        end
    end
end