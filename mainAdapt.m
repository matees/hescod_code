close all;
clear;
warning off;

load ieee802_16e_matrices.mat;

EbNodBs=[0 2 5 8 10 15];
ind = [0.5 1 1.3 1.5 2 2.6 3 4 5];
rates = cell(1,9);
numNiveles = zeros(1,9);

ber=zeros(length(EbNodBs),9);

for ii=1:length(EbNodBs)
    
    EbNodB = EbNodBs(ii);
    
    %Configuracion 1 (0.5)
    hb = ieee802_16e_1_2;
    numNiveles(1) = 2;
    rates{1} = '1/2';
    ber(ii,1) = ldpc(hb, numNiveles(1), EbNodB);
    
    %Configuracion 2 (1)
    hb = ieee802_16e_1_2;
    numNiveles(2) = 4;
    rates{2} = '1/2';
    ber(ii,2) = ldpc(hb, numNiveles(2), EbNodB);
    
    %Configuracion 3 (1.33)
    hb = ieee802_16e_2_3A;
    numNiveles(3) = 4;
    rates{3} = '2/3';
    ber(ii,3) = ldpc(hb, numNiveles(3), EbNodB);
    
    %Configuracion 4 (1.5)
    hb = ieee802_16e_3_4A;
    numNiveles(4) = 4;
    rates{4} = '3/4';
    ber(ii,4) = ldpc(hb, numNiveles(4), EbNodB);
    
    %Configuracion 4 (2)
    hb = ieee802_16e_1_2;
    numNiveles(5) = 16;
    rates{5}='1/2';
    ber(ii,5) = ldpc(hb, numNiveles(5), EbNodB);
    
    %Configuracion 4 (2.66)
    hb = ieee802_16e_2_3A;
    numNiveles(6) = 16;
    rates{6} = '2/3';
    ber(ii,6) = ldpc(hb, numNiveles(6), EbNodB);
    
    %Configuracion 4 (3)
    hb = ieee802_16e_3_4A;
    numNiveles(7) = 16;
    rates{7} = '3/4';
    ber(ii,7) = ldpc(hb, numNiveles(7), EbNodB);
    
    %Configuracion 4 (4)
    hb = ieee802_16e_2_3A;
    numNiveles(8) = 64;
    rates{8} = '2/3';
    ber(ii,8) = ldpc(hb, numNiveles(8), EbNodB);
    
    %Configuracion 4 (5)
    hb = ieee802_16e_5_6;
    numNiveles(9) = 64;
    rates{9}='5/6';
    ber(ii,9) = ldpc(hb, numNiveles(9), EbNodB);
    
end

save('datosAdapt.mat','ind','ber','rates','numNiveles');
warning on;

