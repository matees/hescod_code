function [modulatedSymbols,k,symbols,sizePadding] = modulate(encodedBits,params)

mods = params.mod;
niveles = params.niveles;
order = params.order;
sizePadding = 0;

switch mods
    
    case 'pam'
        symbols = fliplr(real(pammod(0:niveles-1,niveles,0,order)));
        
    case 'psk'
        symbols = pskmod(0:niveles-1,niveles,0,order);
        
    case 'qam'
        symbols = qammod(0:niveles-1,niveles,order);
end

k = log2(niveles);
padding = mod(length(encodedBits),k);
if(padding)
    sizePadding = k - padding;
    encodedBits = [encodedBits zeros(1,sizePadding)];
end
bits = reshape(encodedBits,k,[]).';
ind = bi2de(bits,'left-msb');
modulatedSymbols = symbols(ind+1);

