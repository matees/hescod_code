function plotReceivedSymbols(modulatedSymbols, receivedSymbols,symbols,SNRdB,params)

mods = params.mod;
niv = params.niveles;

l = length(modulatedSymbols);
pos = 1:round(l/4):round(l/4)*4;

% Auxiliar variables
colors = min(0.9,0.2+rand(niv,3));
naranja = [1 0.5 0];
angles =2*pi/(2*niv):2*pi/niv:2*pi - (2*pi/(2*niv));
x = 5*cos(angles);
y = 5*sin(angles);

% Create the window where the informations wil be plotted
hfig2=fullfig('Name', 'Constelacion de Simbolos Recibidos');
set(hfig2, 'ToolBar', 'none');

for ii = 1:length(pos)
    
    m = modulatedSymbols{pos(ii)};
    r = receivedSymbols{pos(ii)};
    limit = 0;
    
    % Set the subplot area
    subplot(2,2,ii);
    set(gca,'FontSize',18);
    str = sprintf('%d-%s -- SNR = %d dB',niv,upper(mods),SNRdB(pos(ii)));
    title(str,'FontSize',20,'FontWeight','Bold');
    hold on;
    
    for jj = 1:length(symbols)
        
        % Plot the constellation of received symbols
        rjj = r(m==symbols(jj));
        if(isreal(rjj))
            plot(rjj,zeros(1,length(rjj)),'color',colors(jj,:),'marker','*','LineStyle','none');
        else
            plot(real(rjj),imag(rjj),'color',colors(jj,:),...
                'marker','*','LineStyle','none');
        end
        
        % Compute the limits for axes
        ljj = ceil(max(max(real(rjj),imag(rjj))));
        limit = max(limit,ljj);
        
        %Plot the symbols corresponding to the modulation
        plot(real(symbols),imag(symbols),'k*','MarkerSize',10);
                
        %Plot the decision regions for PSK modulations
        if(strcmp(mods,'psk'))
                plot([0 x(jj)],[0 y(jj)],'k-','color',naranja,'LineWidth',2.5); 
        end
        
    end 
    
    xlim([-limit limit]);
    ylim([-limit limit]);
    
    %Plot the decision regions for PAM ans QAM modulations
    if(~strcmp(mods,'psk'))
        maxim = max(real(symbols))-1;
        a = -maxim:2:maxim;
        b = -limit:limit;
        x=(ones(length(b),1)*a)';
        y = repmat(b,length(a),1);
        plot(x',y','k-','color',naranja,'LineWidth',2.5);
        if(strcmp(mods,'qam'))
            plot(y',x','k-','color',naranja,'LineWidth',2.5);
        end
    end
    hold off; 
        
end
