function [] = plotConstelacion(mod,M, order,axes)


switch mod
    case 'psk'
        symbols = pskmod(0:M-1,M,0,order);
        l = 1.5;
        l1=l;
    case 'qam'
        symbols = qammod(0:M-1,M,order);
        l = max(real(symbols))+0.5;
        l1=l;
    case 'pam'
        i=0:M-1;
        symbols =2*i-M+1; 
        l = max(real(symbols))+0.5;
        l1=2;
end

plot(real(symbols),imag(symbols),'r*','MarkerSize',15,'Parent',axes);
hold(axes,'on')
plot(zeros(1,2*l1+1),-l1:l1,'k','LineWidth',1.5,'Parent',axes);
plot(-l:l,zeros(1,2*l+1),'k','LineWidth',1.5,'Parent',axes);
str={};
for ii=1:length(symbols)
        str{ii} = sprintf('s_{%d}',ii-1);
end
text(real(symbols)+0.08,imag(symbols)+0.08,str,'FontSize',15,'Parent',axes);
grid(axes,'on');
title(axes,sprintf('Simbolos de la modulacion %d-%s',M,upper(mod)));
xlim(axes,[-l l]);
ylim(axes,[-l1 l1]);
hold(axes,'off')