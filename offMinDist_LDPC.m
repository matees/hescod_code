
load ieee802_16e_matrices.mat;
hbMatrices={ieee802_16e_1_2,ieee802_16e_2_3A,ieee802_16e_3_4A,ieee802_16e_5_6};

minDist = zeros(1,length(hbMatrices));

for ii = 1:length(hbMatrices)
    % Generar matriz control de paridad del LDPC
    hb = hbMatrices{ii};
    z = 10;
    H = genHexp(hb,z);
    H2 = full(H);
    minDist(ii) = gfweight(H2,'par')
end