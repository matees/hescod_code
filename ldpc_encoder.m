function v = ldpc_encoder(H,u,z)

[m,n] = size(H);
k = n-m;
% In standards ieee 802.11n and ieee 802.16e, the gap value
% is equal to expanding factor z.
ir = m-z; ic = k+z;

%Submatrices for efficient encoding
A = H(1:ir,1:k);
B = H(1:ir,k+1:ic);
T = H(1:ir,ic+1:end);
C = H(ir+1:end,1:k);
%D = H(ir+1:end,k+1:ic);
E = H(ir+1:end,ic+1:end);

%Calculate p1
ET1 = mod(E/T,2);
auT = mod(A*u.',2);
cuT = mod(C*u.',2);
p1T = mod(ET1*auT + cuT,2);

%%% Uncomment if ieee802_16e_3_4B is used %%%
%phi = mod(ET1*B + D,2);
%p1T = mod(phi\p1T,2);

%Calculate p2
Tp2T = mod(auT + B*p1T,2);
p2T = mod(T\Tp2T,2);

%Built the codeword
v = [u p1T.' p2T.'];