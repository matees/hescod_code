function estBits = decodingOp(demodulatedBits,params,sizePadding)

type = params.codeType;

switch type 
    
    case 'NoCoding'
        estBits = demodulatedBits;
        
    case 'Hamming'
        l = params.info;
        k = 2^l - 1- l;
        n = 2^l - 1;
        estBits = decode(demodulatedBits,n,k,'hamming/binary');
        
    case 'Conv.'
        info = params.info;
        bins = zeros(1,length(info));
        for ii = 1:length(info)
            bins(ii) = length(oct2poly(info(ii)));
        end
        M = max(bins);
        t = poly2trellis(M,info);
        estBits = vitdec(demodulatedBits,t,5*M,'trunc','hard');
        
    case 'LDPC'
        hb = params.info;
        z = 80;
        H = genHexp(hb,z);
        ldpcDec = comm.LDPCDecoder(H);
        
        [m,n] = size(H);
        k = n-m;
        nb = length(demodulatedBits)/n;
        estBits = zeros(1,nb*k);
        
        for ii = 1:nb
            llrValues = demodulatedBits((ii-1)*n+1:ii*n);
            estBits((ii-1)*k+1:ii*k) = step(ldpcDec,llrValues).';
        end
        
end

estBits(end-sizePadding+1:end) = [];

